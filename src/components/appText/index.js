import { observableFactory } from 'lemejs';

import template from './template'
import styles from './styles'


const appText = () => {

  const state = observableFactory({
    text: 'A simple, no-overload library for creating reactive (SPA) applications.'
  })

  const methods = () => ({
    updateState() {
      state.set({title: 'Crazy World!!!'})
    }
  })

  return {
    state,
    template,
    styles,
    methods
  }
};

export { appText };
