import { routerFactory } from 'lemejs'
import { appHello } from './components/appHello'
import { appNotFound } from './components/appNotFound'

export const router = routerFactory()

router.add(/^#\/$/, appHello, [])
router.add(/^#\/404$/, appNotFound, []) 

router.set({ initial: '#/'})
router.set({ default: '#/404'})